package com.bilas.guns;

import java.util.Objects;

// technical-tactic characteristics class
public class TTC {
    private Range range;
    private int effectiveRange;
    private boolean hasOptic;
    private boolean hasClip;

    public TTC(Range range, int effectiveRange, boolean hasOptic, boolean hasClip) {
        this.range = range;
        this.effectiveRange = effectiveRange;
        this.hasOptic = hasOptic;
        this.hasClip = hasClip;
    }

    public Range getRange() {
        return range;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public int getEffectiveRange() {
        return effectiveRange;
    }

    public void setEffectiveRange(int effectiveRange) {
        this.effectiveRange = effectiveRange;
    }

    public boolean isHasOptic() {
        return hasOptic;
    }

    public void setHasOptic(boolean hasOptic) {
        this.hasOptic = hasOptic;
    }

    public boolean isHasClip() {
        return hasClip;
    }

    public void setHasClip(boolean hasClip) {
        this.hasClip = hasClip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TTC ttc = (TTC) o;
        return effectiveRange == ttc.effectiveRange &&
                hasOptic == ttc.hasOptic &&
                hasClip == ttc.hasClip &&
                Objects.equals(range, ttc.range);
    }

    @Override
    public int hashCode() {
        return Objects.hash(range, effectiveRange, hasOptic, hasClip);
    }

    @Override
    public String toString() {
        return "TTC{" +
                "range=" + range +
                ", effectiveRange=" + effectiveRange +
                ", hasOptic=" + hasOptic +
                ", hasClip=" + hasClip +
                '}';
    }
}

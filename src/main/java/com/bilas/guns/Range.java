package com.bilas.guns;

import java.util.Objects;

public class Range {
    private int closeRange;
    private int averageRange;
    private int farRange;

    public Range(int closeRange, int averageRange, int farRange) {
        this.closeRange = closeRange;
        this.averageRange = averageRange;
        this.farRange = farRange;
    }

    public int getCloseRange() {
        return closeRange;
    }

    public void setCloseRange(int closeRange) {
        this.closeRange = closeRange;
    }

    public int getAverageRange() {
        return averageRange;
    }

    public void setAverageRange(int averageRange) {
        this.averageRange = averageRange;
    }

    public int getFarRange() {
        return farRange;
    }

    public void setFarRange(int farRange) {
        this.farRange = farRange;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Range range = (Range) o;
        return closeRange == range.closeRange &&
                averageRange == range.averageRange &&
                farRange == range.farRange;
    }

    @Override
    public int hashCode() {
        return Objects.hash(closeRange, averageRange, farRange);
    }

    @Override
    public String toString() {
        return "Range{" +
                "closeRange=" + closeRange +
                ", averageRange=" + averageRange +
                ", farRange=" + farRange +
                '}';
    }
}

package com.bilas.guns;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Gun {
    @SerializedName("model")
    private String modelName;
    private String origin;
    private String material;
    private Handy handy;
    @SerializedName("TTC")
    private TTC technicalTacticCharacteristic;

    public Gun(String modelName, String origin, String material, Handy handy, TTC technicalTacticCharacteristic) {
        this.modelName = modelName;
        this.origin = origin;
        this.material = material;
        this.handy = handy;
        this.technicalTacticCharacteristic = technicalTacticCharacteristic;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Handy getHandy() {
        return handy;
    }

    public void setHandy(Handy handy) {
        this.handy = handy;
    }

    public TTC getTechnicalTacticCharacteristic() {
        return technicalTacticCharacteristic;
    }

    public void setTechnicalTacticCharacteristic(TTC technicalTacticCharacteristic) {
        this.technicalTacticCharacteristic = technicalTacticCharacteristic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gun gun = (Gun) o;
        return Objects.equals(modelName, gun.modelName) &&
                Objects.equals(origin, gun.origin) &&
                Objects.equals(material, gun.material) &&
                handy == gun.handy &&
                Objects.equals(technicalTacticCharacteristic, gun.technicalTacticCharacteristic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(modelName, origin, material, handy, technicalTacticCharacteristic);
    }

    @Override
    public String toString() {
        return "Gun{" +
                "modelName='" + modelName + '\'' +
                ", origin='" + origin + '\'' +
                ", material='" + material + '\'' +
                ", handy=" + handy +
                ", technicalTacticCharacteristic=" + technicalTacticCharacteristic +
                '}';
    }
}

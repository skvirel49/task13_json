package com.bilas;

import com.bilas.guns.Gun;
import com.bilas.guns.Handy;
import com.bilas.guns.Range;
import com.bilas.guns.TTC;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    public static void main(String[] args) {
        Gun gun = new Gun("colt", "usa", "steel", Handy.ONE_HAND, new TTC(new Range(20, 500, 900), 300, false, false));
        String json = GSON.toJson(gun);

        writeJSON(json);
        readJSON();
    }

    private static void writeJSON(String json) {
        try (FileOutputStream fos = new FileOutputStream("object.json")) {
            byte[] buffer = json.getBytes();
            fos.write(buffer, 0, buffer.length);
        } catch (IOException ex) {
            LOGGER.info(ex.getMessage());
        }
    }

    private static void readJSON() {
        try(FileReader reader = new FileReader("/home/skvirel49/git/java/task13_json/object.json"))
        {
            char[] buf = new char[256];
            int c;
            while((c = reader.read(buf))>0){

                if(c < 256){
                    buf = Arrays.copyOf(buf, c);
                }
                System.out.print(buf);
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }
}
